FROM --platform=linux/arm64 ghcr.io/dtcooper/raspberrypi-os:latest

ARG ROOT_VERSION=6.30.06

WORKDIR /root

RUN apt-get update && apt install -y \
    dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev libxft-dev libxext-dev python3 libssl-dev git \
    gfortran libpcre3-dev xlibmesa-glu-dev libftgl-dev libmariadb-dev libfftw3-dev libcfitsio-dev graphviz-dev libavahi-compat-libdnssd-dev libldap2-dev python3-dev libxml2-dev libkrb5-dev libgsl0-dev \
    && rm -rf /var/lib/apt/lists/*

ADD https://root.cern/download/root_v${ROOT_VERSION}.source.tar.gz /root/root_v${ROOT_VERSION}.source.tar.gz

RUN tar xzf root_v${ROOT_VERSION}.source.tar.gz

RUN LDFLAGS="-L/usr/lib/gcc/aarch64-linux-gnu/12/ -latomic" \
    cmake \
        -Dsoversion=ON \
        -Dbuiltin_gsl=ON \
        -Dbuiltin_fftw3=ON \
        -Dbuiltin_cfitsio=ON \
        -DCMAKE_CXX_STANDARD=17 \
        -Droot7=OFF \
        -Dfortran=ON \
        -Droofit=ON \
        -Droofit_multiprocess=ON \
        -Droostats=ON \
        -Dhistfactory=ON \
        -Dminuit2=ON \
        -DCMAKE_CXX_FLAGS=-D__ROOFIT_NOBANNER \
        -Dxrootd=OFF \
        -Dbuiltin_xrootd=OFF \
        -Dbuiltin_pcre=OFF \
        -Dvdt=ON \
        -Dbuiltin_vdt=ON \
        -Dbuiltin_ssl=OFF \
        -Dpyroot=ON \
        -DPYTHON_EXECUTABLE=$(which python3) \
        -DCMAKE_INSTALL_PREFIX=/myroot/ \
        -B build \
        root-${ROOT_VERSION} && \
    LDFLAGS="-L/usr/lib/gcc/aarch64-linux-gnu/12/ -latomic" \
    cmake \
        --build build \
        --target install \
        --parallel $(($(nproc) - 1))
